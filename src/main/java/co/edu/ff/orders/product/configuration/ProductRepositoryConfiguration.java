package co.edu.ff.orders.product.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import co.edu.ff.orders.producto.repositories.ProductRepository;
import co.edu.ff.orders.producto.repositories.SqlProductRepository;

@Configuration
public class ProductRepositoryConfiguration {
	
	 @Bean
	 @Profile ({"dev", "prod", "test"})
	 public ProductRepository productoRepository (JdbcTemplate jdbcTemplate, DataSource dataSource) {
	    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
	             .withTableName("PRODUCTS")
	             .usingGeneratedKeyColumns("id");
	    return new SqlProductRepository(jdbcTemplate, simpleJdbcInsert);
	 }

}
