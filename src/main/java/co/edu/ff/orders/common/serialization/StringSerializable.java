package co.edu.ff.orders.common.serialization;

public interface StringSerializable {
    String valueOf();
}
