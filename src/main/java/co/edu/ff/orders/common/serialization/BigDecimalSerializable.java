package co.edu.ff.orders.common.serialization;

import java.math.BigDecimal;

public interface BigDecimalSerializable {
	
	BigDecimal valueOf();

}
