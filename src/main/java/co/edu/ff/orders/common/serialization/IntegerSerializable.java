package co.edu.ff.orders.common.serialization;

public interface IntegerSerializable {
	
	Integer valueOf();

}
