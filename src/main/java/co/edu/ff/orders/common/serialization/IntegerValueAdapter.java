package co.edu.ff.orders.common.serialization;

import java.util.function.Function;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;

import java.lang.reflect.Type;

public class IntegerValueAdapter<T extends IntegerSerializable> implements GsonAdapter<T>{

	private final Function<Integer, T> functionFactory;
	
	public  IntegerValueAdapter(Function<Integer, T> functionFactory) {
		this.functionFactory = functionFactory;
	}
	
	@Override
	public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		Integer value = json.getAsInt();
		return functionFactory.apply(value);
	}

	@Override
	public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
		Integer value = src.valueOf();
		return new JsonPrimitive(value);
	}

}