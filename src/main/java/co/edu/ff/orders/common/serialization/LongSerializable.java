package co.edu.ff.orders.common.serialization;

public interface LongSerializable {
	
	Long valueOf();

}
