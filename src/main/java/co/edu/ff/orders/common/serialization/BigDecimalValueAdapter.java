package co.edu.ff.orders.common.serialization;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.function.Function;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;

public class BigDecimalValueAdapter<T extends BigDecimalSerializable> implements GsonAdapter<T> {

	private final Function<BigDecimal, T> functionFactory;
	
	public BigDecimalValueAdapter(Function<BigDecimal, T> functionFactory) {
		this.functionFactory = functionFactory;
	}
	@Override
	public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		BigDecimal value = json.getAsBigDecimal();
		return functionFactory.apply(value);
	}

	@Override
	public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
		BigDecimal value = src.valueOf();
		return new JsonPrimitive(value);
	}

}
