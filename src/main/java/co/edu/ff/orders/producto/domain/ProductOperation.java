package co.edu.ff.orders.producto.domain;

public interface ProductOperation {
	
	Product value();
	String errorMessage();
	Boolean isValid();

}
