package co.edu.ff.orders.producto.exceptions;

public abstract class ProductException extends RuntimeException {
	
	public ProductException (String message) {
		super(message);
	}

}
