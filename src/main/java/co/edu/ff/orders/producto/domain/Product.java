package co.edu.ff.orders.producto.domain;

import lombok.Value;

@Value(staticConstructor = "of")
public class Product {
	
	Long id;
	Name nombre;
	Description descripcion;
	BasePrice precioBase;
	TaxRate tasaImpuestos;
	ProductStatusEnum estado;
	InventoryQuantity cantidadInventario;

}
