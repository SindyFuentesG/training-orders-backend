package co.edu.ff.orders.producto.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import co.edu.ff.orders.producto.domain.Product;
import co.edu.ff.orders.producto.domain.ProductOperation;
import co.edu.ff.orders.producto.domain.ProductOperationFailure;
import co.edu.ff.orders.producto.domain.ProductOperationRequest;
import co.edu.ff.orders.producto.domain.ProductOperationSuccess;
import co.edu.ff.orders.producto.exceptions.ProductDoesNotExists;
import co.edu.ff.orders.producto.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServices {
	
	private final ProductRepository repository;
	
	public ProductOperation insertOne(ProductOperationRequest productBody) {
		Product productCreated = repository.insertOne(productBody.getNombre(), productBody.getDescripcion(),
				productBody.getPrecioBase(), productBody.getTasaImpuestos(),
				productBody.getEstado().toString(),
				productBody.getCantidadInventario());
		return ProductOperationSuccess.of(productCreated);
	}
	
	public ProductOperation findById(Long id){
		Optional<Product> product = repository.findById(id);
		if(!product.isPresent()) {
			ProductDoesNotExists exception = ProductDoesNotExists.of(id);
			return ProductOperationFailure.of(exception);
		}
		return ProductOperationSuccess.of(product.get());
	}
	
	public List<Product> findAll(){
		return repository.findAll();
	}
	
	public ProductOperation deleteById(Long id){
		Optional<Product> productDelete = repository.findById(id);
		if(!productDelete.isPresent()) {
			ProductDoesNotExists exception = ProductDoesNotExists.of(id);
			return ProductOperationFailure.of(exception);
		}
		repository.deleteById(id);
		return ProductOperationSuccess.of(productDelete.get());
	}
	
	public ProductOperation updateById(Long id, ProductOperationRequest productBody) {
		Optional<Product> productToUpdate = repository.findById(id);
		if(!productToUpdate.isPresent()) {
			ProductDoesNotExists exception = ProductDoesNotExists.of(id);
			return ProductOperationFailure.of(exception);
		}
		Product productUpdate =repository.updateById(id, productBody.getNombre(), productBody.getDescripcion(),
				productBody.getPrecioBase(), productBody.getTasaImpuestos(),
				productBody.getEstado().toString(),
				productBody.getCantidadInventario());
		return ProductOperationSuccess.of(productUpdate);
	}

}
