package co.edu.ff.orders.producto.domain;

import co.edu.ff.orders.common.Preconditions;
import co.edu.ff.orders.common.serialization.IntegerSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class InventoryQuantity implements IntegerSerializable {
	
	Integer value;
	
	private InventoryQuantity (Integer value) {
		Preconditions.checkNotNull(value);
		Preconditions.checkArgument(value >= 0);
		this.value = value;
	}
	
	@Override
	public Integer valueOf() {
		return value;
	}

}
