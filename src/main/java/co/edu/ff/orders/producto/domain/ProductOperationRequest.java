package co.edu.ff.orders.producto.domain;

import co.edu.ff.orders.common.Preconditions;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductOperationRequest {

	Name nombre;
	Description descripcion;
	BasePrice precioBase;
	TaxRate tasaImpuestos;
	ProductStatusEnum estado;
	InventoryQuantity cantidadInventario;

	public ProductOperationRequest(Name nombre, Description descripcion, BasePrice precioBase, TaxRate tasaImpuestos,
			ProductStatusEnum estado, InventoryQuantity cantidadInventario) {
		Preconditions.checkNotNull(nombre);
		Preconditions.checkNotNull(descripcion);
		Preconditions.checkNotNull(precioBase);
		Preconditions.checkNotNull(tasaImpuestos);
		Preconditions.checkNotNull(estado);
		Preconditions.checkNotNull(cantidadInventario);
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precioBase = precioBase;
		this.tasaImpuestos = tasaImpuestos;
		this.estado = estado;
		this.cantidadInventario = cantidadInventario;

	}

}
