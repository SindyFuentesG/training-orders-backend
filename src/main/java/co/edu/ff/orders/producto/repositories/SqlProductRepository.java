package co.edu.ff.orders.producto.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import co.edu.ff.orders.producto.domain.BasePrice;
import co.edu.ff.orders.producto.domain.Description;
import co.edu.ff.orders.producto.domain.InventoryQuantity;
import co.edu.ff.orders.producto.domain.Name;
import co.edu.ff.orders.producto.domain.Product;
import co.edu.ff.orders.producto.domain.ProductStatusEnum;
import co.edu.ff.orders.producto.domain.TaxRate;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SqlProductRepository implements ProductRepository {

	private final JdbcTemplate jdbcTemplate;
	private final SimpleJdbcInsert simpleJdbcInsert;

	private final RowMapper<Product> rowMapper = (resultSet, i) -> {
		Long id1 = resultSet.getLong("id");
		Name name = Name.of(resultSet.getString("name"));
		Description description = Description.of(resultSet.getString("description"));
		BasePrice basePrice = BasePrice.of(resultSet.getBigDecimal("base_price"));
		TaxRate taxRate = TaxRate.of(resultSet.getBigDecimal("tax_rate"));
		ProductStatusEnum productStatus = ProductStatusEnum.valueOf(resultSet.getString("status"));
		InventoryQuantity inventoryQuantity = InventoryQuantity.of(resultSet.getInt("inventory_quantity"));
		return Product.of(id1, name, description, basePrice, taxRate, productStatus, inventoryQuantity);
	};

	@Override
	public Product insertOne(Name name, Description description, BasePrice basePrice, TaxRate taxRate,
			String productStatus, InventoryQuantity inventoryQuantity) {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("name", name.getValue());
		parameters.put("description", description.getValue());
		parameters.put("base_price", basePrice.getValue());
		parameters.put("tax_rate", taxRate.getValue());
		parameters.put("status", productStatus);
		parameters.put("inventory_quantity", inventoryQuantity.getValue());
		Number number = simpleJdbcInsert.executeAndReturnKey(parameters);
		long id = number.longValue();
		return Product.of(id, name, description, basePrice, taxRate, ProductStatusEnum.valueOf(productStatus), inventoryQuantity);
	}

	@Override
	public Optional<Product> findById(Long id) {
		String sql = "SELECT id, name, description, base_price, tax_rate, status, inventory_quantity FROM PRODUCTS WHERE id = ?";
		Object[] objects = { id };
		try {
			Product productCreated = jdbcTemplate.queryForObject(sql, objects, rowMapper);
			return Optional.ofNullable(productCreated);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	@Override
	public List<Product> findAll() {
		String sql = "SELECT id, name, description, base_price, tax_rate, status, inventory_quantity FROM PRODUCTS";
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public void deleteById(Long id) {
		String sql = "DELETE FROM PRODUCTS WHERE id = ?";
		Object[] objects = { id };
		jdbcTemplate.update(sql, objects);
	}

	@Override
	public Product updateById(Long id, Name name, Description description, BasePrice basePrice, TaxRate taxRate,
			String productStatus, InventoryQuantity inventoryQuantity) {
		String sql = "UPDATE PRODUCTS SET name = ?,  description = ?, base_price = ?, tax_rate = ?, status = ?, inventory_quantity = ? WHERE id = ?";
		Object[] objects = { name.getValue(), description.getValue(), basePrice.getValue(), taxRate.getValue(),
				productStatus, inventoryQuantity.getValue(), id };
		jdbcTemplate.update(sql,objects);
		return Product.of(id, name, description, basePrice, taxRate, ProductStatusEnum.valueOf(productStatus), inventoryQuantity);
	}

}
