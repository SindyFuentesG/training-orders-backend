package co.edu.ff.orders.producto.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import co.edu.ff.orders.producto.domain.BasePrice;
import co.edu.ff.orders.producto.domain.Description;
import co.edu.ff.orders.producto.domain.InventoryQuantity;
import co.edu.ff.orders.producto.domain.Name;
import co.edu.ff.orders.producto.domain.Product;
import co.edu.ff.orders.producto.domain.TaxRate;

@Repository
public interface ProductRepository {

	Product insertOne(Name name, Description description, BasePrice basePrice, TaxRate taxRate,
			String productStatus, InventoryQuantity inventoryQuantity);
	
	Optional<Product> findById(Long id);
	
	List<Product> findAll();
	
	void deleteById(Long id);
	
	Product updateById(Long id, Name name, Description description, BasePrice basePrice, TaxRate taxRate,
			String productStatus, InventoryQuantity inventoryQuantity);

}
