package co.edu.ff.orders.producto.exceptions;

import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode (callSuper = true)
@Value(staticConstructor = "of")
public class ProductDoesNotExists extends ProductException{

	Long id;
	
	private ProductDoesNotExists (Long id) {
		super(String.format("Product %s does not exists", id));
		this.id = id;
	}
}
