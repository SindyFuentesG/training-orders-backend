package co.edu.ff.orders.producto.domain;

public enum ProductStatusEnum {
	
	BORRADOR,
	PUBLICADO;
	
}
