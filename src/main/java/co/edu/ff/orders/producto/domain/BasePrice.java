package co.edu.ff.orders.producto.domain;

import java.math.BigDecimal;

import co.edu.ff.orders.common.Preconditions;
import co.edu.ff.orders.common.serialization.BigDecimalSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class BasePrice implements BigDecimalSerializable {
	
	BigDecimal value;
	
	private BasePrice (BigDecimal value) {
		Preconditions.checkNotNull(value);
		Preconditions.checkArgument(!(value.compareTo(BigDecimal.ZERO) == -1));
		this.value = value;
	}

	@Override
	public BigDecimal valueOf() {
		return value;
	}

}
