package co.edu.ff.orders.producto.domain;

import org.apache.commons.lang3.StringUtils;

import co.edu.ff.orders.common.Preconditions;
import co.edu.ff.orders.common.serialization.StringSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class Description implements StringSerializable {
	
	String value;
	
	private Description(String value) {
		Preconditions.checkNotNull(value);
		Preconditions.checkArgument(value.length() <= 280);
		Preconditions.checkArgument(!StringUtils.isBlank(value));
		this.value = value;
	}

	@Override
	public String valueOf() {
		return value;
	}

}
