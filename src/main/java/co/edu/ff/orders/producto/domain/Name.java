package co.edu.ff.orders.producto.domain;

import org.apache.commons.lang3.StringUtils;

import co.edu.ff.orders.common.Preconditions;
import co.edu.ff.orders.common.serialization.StringSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class Name implements StringSerializable{
	
	String value;
	
	private Name (String value) {
		Preconditions.checkNotNull(value);
		Preconditions.checkArgument(value.length() <= 100);
		Preconditions.checkArgument(!StringUtils.isBlank(value));
		this.value = value;
	}

	@Override
	public String valueOf() {
		return value;
	}

}
