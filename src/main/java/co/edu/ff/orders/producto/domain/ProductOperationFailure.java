package co.edu.ff.orders.producto.domain;

import co.edu.ff.orders.producto.exceptions.ProductException;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductOperationFailure implements ProductOperation {
	ProductException exception;

	public Product value() {
		return null;
	}

	@Override
	public String errorMessage() {
		return exception.getMessage();
	}

	@Override
	public Boolean isValid() {
		return false;
	}
	

}
