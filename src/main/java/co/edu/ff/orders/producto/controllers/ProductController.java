package co.edu.ff.orders.producto.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.ff.orders.producto.domain.Product;
import co.edu.ff.orders.producto.domain.ProductOperation;
import co.edu.ff.orders.producto.domain.ProductOperationRequest;
import co.edu.ff.orders.producto.services.ProductServices;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {

	private final ProductServices services;

	@PostMapping
	public ResponseEntity<ProductOperation> createProduct(@RequestBody ProductOperationRequest productBody) {
		ProductOperation productOperation = services.insertOne(productBody);
		if(productOperation.isValid()) {
			return ResponseEntity.ok(productOperation);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(productOperation);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductOperation> getProductById(@PathVariable Long id){
		ProductOperation productOperation = services.findById(id);
		if(productOperation.isValid()) {
			return ResponseEntity.ok(productOperation);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(productOperation);
	}
	
	@GetMapping
	public List<Product> findAllProduct(){
		return services.findAll();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<ProductOperation> deleteProductById(@PathVariable Long id){
		ProductOperation productOperation = services.deleteById(id);
		if(productOperation.isValid()) {
			return ResponseEntity.ok(productOperation);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(productOperation);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ProductOperation> updateById(@PathVariable Long id, @RequestBody ProductOperationRequest productBody){
		ProductOperation productOperation = services.updateById(id, productBody);
		if(productOperation.isValid()) {
			return ResponseEntity.ok(productOperation);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(productOperation);
	}

}
