package co.edu.ff.orders.utilities;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Streams {
	
	public static void main(String[] args) {
        List<Pizza> pizzaList = Arrays.asList(
                new Pizza("Básica", Size.SMALL, 600),
                new Pizza("Familiar", Size.LARGE, 1800),
                new Pizza("Vegetariana", Size.LARGE, 860),
                new Pizza("Solo queso", Size.MEDIUM, 1000),
                new Pizza("Hawaiana", Size.SMALL, 1200),
                new Pizza("Extra carnes", Size.LARGE, 2100),
                new Pizza("Pollo", Size.SMALL, 900),
                new Pizza("Pollo + tocineta", Size.MEDIUM, 1500),
                new Pizza("Pollo + Jamon", Size.MEDIUM, 1300)
        );
    
    /*
     * 1. Obtener todas las pizzas de tamaño "MEDIUM"
     */
        List<Pizza> pizzasMedium = pizzaList.stream()
        		.filter(pizza -> Size.MEDIUM.equals(pizza.getSize()))
        		.collect(Collectors.toList());
        System.out.println("Pizzas Medium: " + pizzasMedium);
    
    /*
     * 2. Obtener todas las pizzas que las calorias esten entre 700 y 1500
     */
        List<Pizza> pizzasCaloriasRango = pizzaList.stream()
        		.filter(pizza -> pizza.getCalories() >= 700 && pizza.getCalories() <= 1500)
        		.collect(Collectors.toList());
        System.out.println("Pizzas con calorias entre 700 y 1500: "+ pizzasCaloriasRango);
    /*
     * 3. Obtener las 3 pizzas con más calorias
     */
        List<Pizza> pizzasMasCalorias = pizzaList.stream()
        		.sorted(Comparator.comparingInt(Pizza::getCalories).reversed())
        		.limit(3)
        		.collect(Collectors.toList());
        System.out.println("Pizzas con mas calorias: "+ pizzasMasCalorias);		
     /*
     * 4. Obtener las 2 pizzas con menos calorias
     */
        List<Pizza> pizzasMenosCalorias = pizzaList.stream()
        		.sorted(Comparator.comparingInt(Pizza::getCalories))
        		.limit(2)
        		.collect(Collectors.toList());
        System.out.println("Pizzas con menos calorias: "+ pizzasMenosCalorias);		
    /*
     * 5. Del numeral 2 obtener las 2 pizzas con mas calorias
     */
        List<Pizza> pizzasMasCaloriasRango = pizzasCaloriasRango.stream()
        		.sorted(Comparator.comparingInt(Pizza::getCalories).reversed())
        		.limit(2)
        		.collect(Collectors.toList());
        System.out.println("Pizzas con mas calorias rango: "+ pizzasMasCaloriasRango);	
    /*
     * 5. Agrupar las pizzas por tamaño
     */
        Map<Size, List<Pizza>> pizzasBySize = new HashMap<>();
        pizzasBySize = pizzaList.stream()
    		   .collect(Collectors.groupingBy(Pizza::getSize));
        System.out.println("Pizzas agrupadas por tamaño: " + pizzasBySize);
       
    
    /*
     * 6. Agrupar las pizzas por los siguientes grupos: 
     * de 0 a 1000 calorias
     * de 1001 a 2000 calorias
     * de 2001 a 3000 calorias
     */
      Function<Pizza, Calories> pizzaByCalories = pizza -> Calories.getTypeCalories(pizza.getCalories());
      Map<Calories, List<Pizza>> pizzasByCaloriesList = new HashMap<>();
      pizzasByCaloriesList = pizzaList.stream()
  		   .collect(Collectors.groupingBy(pizzaByCalories));
      System.out.println("Pizzas agrupadas por cantidad de calorias: " + pizzasByCaloriesList);

  }
	

  public enum Size {
      SMALL,
      MEDIUM,
      LARGE
  }
  
  public enum Calories {
	  LOW,
	  MODERATE,
	  HIGH,
	  UNKNOWN;
	  
	  public static Calories getTypeCalories (Integer caloriesQuantity) {
		  if(caloriesQuantity >= 0 && caloriesQuantity <= 1000) {
    		  return LOW;
    	  }else if(caloriesQuantity >= 1001 && caloriesQuantity <= 2000) {
    		  return MODERATE;
    	  }else if(caloriesQuantity >= 2001 && caloriesQuantity <= 3000) {
    		  return HIGH;
    	  }else {
    		  return UNKNOWN;
    	  }
	  }
	  
  }

  public static class Pizza {
      private final String name;
      private final Size size;
      private final Integer calories;

      public Pizza(String name, Size size, Integer calories) {
          this.name = name;
          this.size = size;
          this.calories = calories;
      }

      public Size getSize() {
          return size;
      }

      public String getName() {
          return name;
      }

      public Integer getCalories() {
          return calories;
      }

      @Override
      public String toString() {
          return String.format("Pizza{%s, %s, %s}", name, size, calories);
      }
  }

}
