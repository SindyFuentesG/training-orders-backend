package co.edu.ff.orders.user.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;

class UsernameTest {

	@Test
	void of() {
	}

	@TestFactory
    @DisplayName("Debería crear username validos")
    Stream<DynamicTest> isShouldPass(){
    	return Stream.of(
    		"SindyTFG",
    		"sindyFuentes",
    		"sindy fuentes"
    	).map(username -> {
    		String testName = String.format("debería ser valido para el userName: %s", username);
    		Executable executable = () -> {
    			ThrowingSupplier<Username> usernameThrowingSupplier = () -> Username.of(username);
    			assertAll(
    					() -> assertDoesNotThrow(usernameThrowingSupplier),
    					() -> assertNotNull(usernameThrowingSupplier.get())
    			);
    		};
    		return DynamicTest.dynamicTest(testName, executable);
    	});
    }
	

}