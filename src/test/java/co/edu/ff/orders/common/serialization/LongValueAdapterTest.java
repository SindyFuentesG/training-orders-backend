package co.edu.ff.orders.common.serialization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import co.edu.ff.orders.common.serialization.IntegerValueAdapterTest.IntegerAdapterTest;
import lombok.Value;

class LongValueAdapterTest {
	
	static Gson gson;
	
	@Test
	void of() {
	}
	
	@Value(staticConstructor = "of")
	public static class LongAdapterTest implements LongSerializable {
		Long value;
		
		@Override
		public Long valueOf() {
			return value;
		}	
	}
	
	@BeforeAll
	static void setUp() {
		gson = new GsonBuilder()
				.registerTypeAdapter(LongAdapterTest.class, new LongValueAdapter<>(LongAdapterTest::of))
				.create();
	}
	
    @Test
    void serialize() {
    	Long longValue = 3000000L;
    	LongAdapterTest longAdapterTest = LongAdapterTest.of(longValue);
    	String actualJsonValue = gson.toJson(longAdapterTest);
    	String expectedJsonValue = String.format("%s", longValue);
    	assertEquals(actualJsonValue, expectedJsonValue);
    }
    
    @Test
    void deserialize() {
    	Long longValue = 3000000L;
    	String jsonValue = String.format("%s", longValue);
    	LongAdapterTest actualValue = gson.fromJson(jsonValue, LongAdapterTest.class);
    	LongAdapterTest expectedValue = LongAdapterTest.of(longValue);
    	assertEquals(actualValue.valueOf(), expectedValue.valueOf());
    }
	

}
