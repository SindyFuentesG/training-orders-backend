package co.edu.ff.orders.common.serialization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import co.edu.ff.orders.common.serialization.IntegerValueAdapterTest.IntegerAdapterTest;
import lombok.Value;

class BigDecimalValueAdapterTest {
	
	static Gson gson;
	
	@Test
	void of() {
	}
	
	@Value(staticConstructor = "of")
	public static class BigDecimalAdapterTest implements BigDecimalSerializable {
		BigDecimal value;
		
		@Override
		public BigDecimal valueOf() {
			return value;
		}	
	}
	
	@BeforeAll
	static void setUp() {
		gson = new GsonBuilder()
				.registerTypeAdapter(BigDecimalAdapterTest.class, new BigDecimalValueAdapter<>(BigDecimalAdapterTest::of))
				.create();
	}
	
    @Test
    void serialize() {
    	BigDecimal bigDecimalValue = new BigDecimal (13000.98);
    	BigDecimalAdapterTest bigDecimalAdapterTest = BigDecimalAdapterTest.of(bigDecimalValue);
    	String actualJsonValue = gson.toJson(bigDecimalAdapterTest);
    	String expectedJsonValue = String.format("%s", bigDecimalValue);
    	assertEquals(actualJsonValue, expectedJsonValue);
    }
    
    @Test
    void deserialize() {
    	BigDecimal bigDecimalValue = new BigDecimal (13000.98);
    	String jsonValue = String.format("%s", bigDecimalValue);
    	BigDecimalAdapterTest actualValue = gson.fromJson(jsonValue, BigDecimalAdapterTest.class);
    	BigDecimalAdapterTest expectedValue = BigDecimalAdapterTest.of(bigDecimalValue);
    	assertEquals(actualValue.valueOf(), expectedValue.valueOf());
    }
	

}
