package co.edu.ff.orders.common.serialization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Value;

class IntegerValueAdapterTest {
	
	static Gson gson;
	
	@Test
	void of() {
	}
	
	@Value(staticConstructor = "of")
	public static class IntegerAdapterTest implements IntegerSerializable {
		Integer value;
		
		@Override
		public Integer valueOf() {
			return value;
		}	
	}
	
	@BeforeAll
	static void setUp() {
		gson = new GsonBuilder()
				.registerTypeAdapter(IntegerAdapterTest.class, new IntegerValueAdapter<>(IntegerAdapterTest::of))
				.create();
	}
	
    @Test
    void serialize() {
    	Integer integerValue = 100;
    	IntegerAdapterTest integerAdapterTest = IntegerAdapterTest.of(integerValue);
    	String actualJsonValue = gson.toJson(integerAdapterTest);
    	String expectedJsonValue = String.format("%s", integerValue);
    	assertEquals(actualJsonValue, expectedJsonValue);
    }
    
    @Test
    void deserialize() {
    	Integer integerValue = 100;
    	String jsonValue = String.format("%s", integerValue);
    	IntegerAdapterTest actualValue = gson.fromJson(jsonValue, IntegerAdapterTest.class);
    	IntegerAdapterTest expectedValue = IntegerAdapterTest.of(integerValue);
    	assertEquals(actualValue.valueOf(), expectedValue.valueOf());
    }
    

}
