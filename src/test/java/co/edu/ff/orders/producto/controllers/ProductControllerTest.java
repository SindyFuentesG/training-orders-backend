package co.edu.ff.orders.producto.controllers;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;

import co.edu.ff.orders.producto.domain.BasePrice;
import co.edu.ff.orders.producto.domain.Description;
import co.edu.ff.orders.producto.domain.InventoryQuantity;
import co.edu.ff.orders.producto.domain.Name;
import co.edu.ff.orders.producto.domain.Product;
import co.edu.ff.orders.producto.domain.ProductOperation;
import co.edu.ff.orders.producto.domain.ProductOperationFailure;
import co.edu.ff.orders.producto.domain.ProductOperationRequest;
import co.edu.ff.orders.producto.domain.ProductOperationSuccess;
import co.edu.ff.orders.producto.domain.ProductStatusEnum;
import co.edu.ff.orders.producto.domain.TaxRate;
import co.edu.ff.orders.producto.exceptions.ProductDoesNotExists;
import co.edu.ff.orders.producto.services.ProductServices;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductServices productServices;

	@Autowired
	private Gson gson;

	@Test
	void findProductNotExist() throws Exception {
		when(productServices.findById(anyLong())).thenAnswer(new Answer<ProductOperation>() {
			public ProductOperation answer(InvocationOnMock invocation) {
				Long args = invocation.getArgument(0);
				return ProductOperationFailure.of(ProductDoesNotExists.of(args));
			}
		});

		MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/1");
		this.mockMvc.perform(servletRequestBuilder).andDo(print()).andExpect(status().is4xxClientError());
	}

	@Test
	void findProduct() throws Exception {
		Product product = Product.of(1L, Name.of("Computador"), Description.of("Computador portatil"),
				BasePrice.of(new BigDecimal(20000.87)), TaxRate.of(new BigDecimal(0.8)), ProductStatusEnum.PUBLICADO,
				InventoryQuantity.of(100));
		ProductOperationSuccess operationSuccess = ProductOperationSuccess.of(product);

		String productJson = this.gson.toJson(operationSuccess);
		when(productServices.findById(anyLong())).thenReturn(ProductOperationSuccess.of(product));

		MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/1");
		this.mockMvc.perform(servletRequestBuilder).andDo(print()).andExpect(status().isOk())
				.andExpect(content().json(productJson));
	}

	@Test
	void createProduct() throws Exception {
		String name = "Computador";
		String description = "Computador portatil";
		BigDecimal basePrice = new BigDecimal(20000.87);
		BigDecimal taxRate = new BigDecimal(0.8);
		Integer inventoryQuantity = 100;
		String jsonRequest = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);
		String jsonResponse = String.format(
				"{\"value\":{\"id\": 1, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);

		ProductOperationRequest productBodyMock = ArgumentMatchers.any(ProductOperationRequest.class);

		when(productServices.insertOne(productBodyMock)).thenReturn(ProductOperationSuccess.of(Product.of(1L,
				Name.of("Computador"), Description.of("Computador portatil"), BasePrice.of(new BigDecimal(20000.87)),
				TaxRate.of(new BigDecimal(0.8)), ProductStatusEnum.PUBLICADO, InventoryQuantity.of(100))));

		mockMvc.perform(post("/api/v1/products/").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().json(jsonResponse));

	}

	@Test
	void deleteProductDontExists() throws Exception {
		when(productServices.deleteById(anyLong())).thenAnswer(new Answer<ProductOperation>() {
			public ProductOperation answer(InvocationOnMock invocation) {
				Long args = invocation.getArgument(0);
				return ProductOperationFailure.of(ProductDoesNotExists.of(args));
			}
		});

		MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.delete("/api/v1/products/1");
		this.mockMvc.perform(servletRequestBuilder).andDo(print()).andExpect(status().is4xxClientError());
	}

	@Test
	void deleteProduct() throws Exception {
		Product product = Product.of(1L, Name.of("Computador"), Description.of("Computador portatil"),
				BasePrice.of(new BigDecimal(20000.87)), TaxRate.of(new BigDecimal(0.8)),  ProductStatusEnum.PUBLICADO,
				InventoryQuantity.of(100));
		ProductOperationSuccess operationSuccess = ProductOperationSuccess.of(product);

		String productJson = this.gson.toJson(operationSuccess);
		when(productServices.deleteById(anyLong())).thenReturn(ProductOperationSuccess.of(product));

		MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.delete("/api/v1/products/1");
		this.mockMvc.perform(servletRequestBuilder).andDo(print()).andExpect(status().isOk())
				.andExpect(content().json(productJson));
	}

	@Test
	void updateProductDontExists() throws Exception {
		String name = "Computador";
		String description = "Computador portatil";
		BigDecimal basePrice = new BigDecimal(20000.87);
		BigDecimal taxRate = new BigDecimal(0.8);
		Integer inventoryQuantity = 100;

		String jsonRequest = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);

		when(productServices.updateById(anyLong(), any(ProductOperationRequest.class)))
				.thenAnswer(new Answer<ProductOperation>() {
					public ProductOperation answer(InvocationOnMock invocation) {
						Long args = invocation.getArgument(0);
						return ProductOperationFailure.of(ProductDoesNotExists.of(args));
					}
				});

		mockMvc.perform(put("/api/v1/products/1").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
				.andDo(print()).andExpect(status().is4xxClientError());
	}

	@Test
	void updateProduct() throws Exception {
		Name name = Name.of("Computador");
		Description description = Description.of("Computador portatil");
		BasePrice basePrice = BasePrice.of(new BigDecimal(20000.87));
		TaxRate taxRate = TaxRate.of(new BigDecimal(0.8));
		InventoryQuantity inventoryQuantity = InventoryQuantity.of(100);

		String jsonRequest = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				name.getValue(), description.getValue(), basePrice.getValue(), taxRate.getValue(), ProductStatusEnum.PUBLICADO, inventoryQuantity.getValue());
		
		String jsonResponse = String.format(
				"{\"value\":{\"id\": 1, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				name.getValue(), description.getValue(), basePrice.getValue(), taxRate.getValue(), ProductStatusEnum.PUBLICADO, inventoryQuantity.getValue());

		when(productServices.updateById(anyLong(), any(ProductOperationRequest.class)))
		.thenAnswer(new Answer<ProductOperation>() {
			public ProductOperation answer(InvocationOnMock invocation) {
				Long args = invocation.getArgument(0);
				return ProductOperationSuccess
						.of(Product.of(args, name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity));
			}
		});
		
		mockMvc.perform(put("/api/v1/products/1").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
		.andDo(print()).andExpect(status().isOk()).andExpect(content().json(jsonResponse));	
	}

}
