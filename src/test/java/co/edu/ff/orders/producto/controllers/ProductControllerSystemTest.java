package co.edu.ff.orders.producto.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import co.edu.ff.orders.producto.domain.ProductStatusEnum;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
class ProductControllerSystemTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void createAndFindProduct() throws Exception {
		String name = "Computador";
		String description = "Computador portatil";
		BigDecimal basePrice = new BigDecimal(20000.87);
		BigDecimal taxRate = new BigDecimal(0.8);
		Integer inventoryQuantity = 100;
		String jsonRequest = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);
		String jsonResponse = String.format(
				"{\"value\":{\"id\": 1, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);
		mockMvc.perform(post("/api/v1/products/").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
				.andExpect(status().isOk()).andExpect(content().json(jsonResponse));
		mockMvc.perform(get("/api/v1/products/1")).andExpect(status().isOk()).andExpect(content().json(jsonResponse));
	}

	@Test
	void createProductsAndFindAll() throws Exception {
		String name = "Computador";
		String description = "Computador portatil";
		BigDecimal basePrice = new BigDecimal(20000.87);
		BigDecimal taxRate = new BigDecimal(0.8);
		Integer inventoryQuantity = 100;

		String nameTwo = "Celular";
		String descriptionTwo = "Iphone";
		BigDecimal basePriceTwo = new BigDecimal(900000);
		BigDecimal taxRateTwo = new BigDecimal(0.9);
		Integer inventoryQuantityTwo = 300;

		String jsonRequest = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);
		String jsonResponse = String.format(
				"{\"value\":{\"id\": 1, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);

		String jsonRequestTwo = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				nameTwo, descriptionTwo, basePriceTwo, taxRateTwo, ProductStatusEnum.PUBLICADO, inventoryQuantityTwo);
		String jsonResponseTwo = String.format(
				"{\"value\":{\"id\": 2, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				nameTwo, descriptionTwo, basePriceTwo, taxRateTwo, ProductStatusEnum.PUBLICADO, inventoryQuantityTwo);

		String jsonReponseAll = String.format(
				"[{\"id\": 1, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }, {\"id\": 2, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }]",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity, nameTwo, descriptionTwo,
				basePriceTwo, taxRateTwo, ProductStatusEnum.PUBLICADO, inventoryQuantityTwo);

		mockMvc.perform(post("/api/v1/products/").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
				.andExpect(status().isOk()).andExpect(content().json(jsonResponse));
		mockMvc.perform(post("/api/v1/products/").contentType(MediaType.APPLICATION_JSON).content(jsonRequestTwo))
				.andExpect(status().isOk()).andExpect(content().json(jsonResponseTwo));

		mockMvc.perform(get("/api/v1/products/")).andExpect(status().isOk()).andExpect(content().json(jsonReponseAll));
	}

	@Test
	void deleteProductAndFindAll() throws Exception {
		String name = "Computador";
		String description = "Computador portatil";
		BigDecimal basePrice = new BigDecimal(20000.87);
		BigDecimal taxRate = new BigDecimal(0.8);
		Integer inventoryQuantity = 100;

		String nameTwo = "Celular";
		String descriptionTwo = "Iphone";
		BigDecimal basePriceTwo = new BigDecimal(900000);
		BigDecimal taxRateTwo = new BigDecimal(0.9);
		Integer inventoryQuantityTwo = 300;

		String jsonRequest = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);
		String jsonResponse = String.format(
				"{\"value\":{\"id\": 1, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				name, description, basePrice, taxRate, ProductStatusEnum.PUBLICADO, inventoryQuantity);

		String jsonRequestTwo = String.format(
				"{\"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }",
				nameTwo, descriptionTwo, basePriceTwo, taxRateTwo, ProductStatusEnum.PUBLICADO, inventoryQuantityTwo);
		String jsonResponseTwo = String.format(
				"{\"value\":{\"id\": 2, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }}",
				nameTwo, descriptionTwo, basePriceTwo, taxRateTwo, ProductStatusEnum.PUBLICADO, inventoryQuantityTwo);

		String jsonReponseAll = String.format(
				"[{\"id\": 2, \"nombre\": \"%s\", \"descripcion\": \"%s\", \"precioBase\": %s, \"tasaImpuestos\": %s, \"estado\": \"%s\", \"cantidadInventario\": %s }]",
				nameTwo, descriptionTwo, basePriceTwo, taxRateTwo, ProductStatusEnum.PUBLICADO, inventoryQuantityTwo);

		mockMvc.perform(post("/api/v1/products/").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
				.andExpect(status().isOk()).andExpect(content().json(jsonResponse));
		mockMvc.perform(post("/api/v1/products/").contentType(MediaType.APPLICATION_JSON).content(jsonRequestTwo))
				.andExpect(status().isOk()).andExpect(content().json(jsonResponseTwo));

		mockMvc.perform(delete("/api/v1/products/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().json(jsonResponse));
		
		mockMvc.perform(get("/api/v1/products/")).andDo(print()).andExpect(status().isOk()).andExpect(content().json(jsonReponseAll));
	}

}
